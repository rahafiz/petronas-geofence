//
//  Library.h
//  Library
//
//  Created by Wan Rahafiz Wan Abdul Rahim on 30/10/2019.
//  Copyright © 2019 Wan Rahafiz Wan Abdul Rahim. All rights reserved.
//

#import <Foundation/Foundation.h>

//! Project version number for Library.
FOUNDATION_EXPORT double LibraryVersionNumber;

//! Project version string for Library.
FOUNDATION_EXPORT const unsigned char LibraryVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <Library/PublicHeader.h>


