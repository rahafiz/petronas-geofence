//
//  HomeViewModel.swift
//  Geofence
//
//  Created by Wan Rahafiz Wan Abdul Rahim on 27/10/2019.
//  Copyright © 2019 Wan Rahafiz Wan Abdul Rahim. All rights reserved.
//

import Foundation
import CoreLocation

protocol HomeViewModelInputs {
    
}

protocol HomeViewModelOutputs {
    
    var regionLat: Box<CLLocationDegrees?> { get }
    var regionLong: Box<CLLocationDegrees?> { get }
    var geotifications: Box<[Geotification]> { get }
    
    var regionSet: Box<Bool> { get }
    
}

protocol HomeViewModelType {
    var inputs: HomeViewModelInputs { get }
    var outputs: HomeViewModelOutputs { get }
}

class HomeViewModel: HomeViewModelInputs, HomeViewModelOutputs, HomeViewModelType {
    let regionLat: Box<CLLocationDegrees?> = Box(3.1412)
    let regionLong: Box<CLLocationDegrees?> = Box(101.68653)
    let geotifications: Box<[Geotification]> = Box([])
    
    let regionSet: Box<Bool> = Box(false)
    
    var inputs: HomeViewModelInputs { return self }
    var outputs: HomeViewModelOutputs { return self }
    
    init() {
       
    }
    
}
