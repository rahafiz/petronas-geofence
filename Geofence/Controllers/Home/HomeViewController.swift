//
//  HomeViewController.swift
//  Geofence
//
//  Created by Wan Rahafiz Wan Abdul Rahim on 25/10/2019.
//  Copyright © 2019 Wan Rahafiz Wan Abdul Rahim. All rights reserved.
//

import UIKit
import CoreLocation
import MapKit

struct PreferencesKeys {
    static let savedItems = "savedItems"
}

class HomeViewController: UIViewController {

    @IBOutlet weak var addButton: UIButton!
    @IBOutlet weak var mapView: MKMapView!
    
    let viewModel: HomeViewModelType = HomeViewModel()
    
    var locationManager = CLLocationManager()


    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupView()
        setupListener()
        setupNavBar()
        
        locationManager.delegate = self
        locationManager.requestAlwaysAuthorization()
        loadAllGeotifications()

    }
    
    private func setupView() {
        
        
    }
    
    func setupListener() {
        
    }
    
    private func setupNavBar() {
        
        addButton.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        addButton.addTarget(self, action: #selector(goToAddGeofence), for: .touchUpInside)
        addButton.setImage(UIImage(named: "icon-add"), for: .normal)
        addButton.backgroundColor = UIColor.clear
    }
    
    @objc func goToAddGeofence() {
        showShortcutPopup()
    }
    
    @objc func goToAddLocation() {
        
        let identifier = String(describing: AddLocationViewController.self)
        guard let vc = vc("Main", identifier: identifier) as? AddLocationViewController else { return }
        vc.delegate = self
        
        navigationController?.pushViewController(
            vc,
            animated: true
        )
    }
    
    // MARK: Other mapview functions
    @IBAction func zoomToCurrentLocation(sender: AnyObject) {
        mapView.zoomToUserLocation()
    }

    
    // MARK: Map overlay functions
    func addRadiusOverlay(forGeotification geotification: Geotification) {
        mapView?.addOverlay(MKCircle(center: geotification.coordinate, radius: geotification.radius))
    }
    
    func removeRadiusOverlay(forGeotification geotification: Geotification) {
        // Find exactly one overlay which has the same coordinates & radius to remove
        guard let overlays = mapView?.overlays else { return }
        for overlay in overlays {
            guard let circleOverlay = overlay as? MKCircle else { continue }
            let coord = circleOverlay.coordinate
            if coord.latitude == geotification.coordinate.latitude && coord.longitude == geotification.coordinate.longitude && circleOverlay.radius == geotification.radius {
                mapView?.removeOverlay(circleOverlay)
                break
            }
        }
    }
    
    func startMonitoring(geotification: Geotification) {
        if !CLLocationManager.isMonitoringAvailable(for: CLCircularRegion.self) {
            showAlert(withTitle:"Error", message: "Geofencing is not supported on this device!")
            return
        }
        
        if CLLocationManager.authorizationStatus() != .authorizedAlways {
            let message = "Your geotification is saved but will only be activated once you grant Geotify permission to access the device location."
            showAlert(withTitle:"Warning", message: message)
        }
        
        let fenceRegion = region(with: geotification)
        locationManager.startMonitoring(for: fenceRegion)
    }
    
    func region(with geotification: Geotification) -> CLCircularRegion {
        let region = CLCircularRegion(center: geotification.coordinate, radius: geotification.radius, identifier: geotification.identifier)
        region.notifyOnEntry = (geotification.eventType == .onEntry)
        region.notifyOnExit = !region.notifyOnEntry
        return region
    }
    
    // MARK: Loading and saving functions
    func loadAllGeotifications() {
        viewModel.outputs.geotifications.value.removeAll()
        let allGeotifications = Geotification.allGeotifications()
        allGeotifications.forEach { add($0) }
    }
    
    func saveAllGeotifications() {
        let encoder = JSONEncoder()
        do {
            let data = try encoder.encode(viewModel.outputs.geotifications.value)
            UserDefaults.standard.set(data, forKey: PreferencesKeys.savedItems)
            
            let title: String = "Success"
            let subTitle: String = "Adding GeoFence"
            let desc: String = "You successfully add the Geofence location to track"
            let buttonText = "Okay"
            let animationName = "success"
            
            generalPopupImageVC(title: title, subTitle: subTitle, desc: desc, buttonText: buttonText, animationName: animationName)
            
        } catch {
            let title = "Ops"
            let subTitle = "Adding GeoFence"
            let desc = "There is slight issue when adding the Geofence location. Let's try again later"
            let buttonText = "Okay"
            let animationName = "failed"
    
            generalPopupImageVC(title: title, subTitle: subTitle, desc: desc, buttonText: buttonText, animationName: animationName)

        }
    }
    
    // MARK: Functions that update the model/associated views with geotification changes
    func add(_ geotification: Geotification) {
        viewModel.outputs.geotifications.value.append(geotification)
        mapView.addAnnotation(geotification)
        addRadiusOverlay(forGeotification: geotification)
    }
    
    func remove(_ geotification: Geotification) {
        guard let index = viewModel.outputs.geotifications.value.index(of: geotification) else { return }
        viewModel.outputs.geotifications.value.remove(at: index)
        mapView.removeAnnotation(geotification)
        removeRadiusOverlay(forGeotification: geotification)
    }
    
    func updateGeotificationsCount() {
        addButton.isEnabled = (viewModel.outputs.geotifications.value.count < 20)
    }
    
    @objc func generalPopupImageVC(title: String, subTitle: String, desc: String, buttonText: String, animationName: String) {
        let vcIdentifier = String(describing: GeneralImagePopupVC.self)
        guard let vc = UIViewController.vc(
            "GeneralPopup",
            identifier: vcIdentifier
            ) as? GeneralImagePopupVC else {
                return
        }
        
        vc.viewModel.outputs.title.value = title
        vc.viewModel.outputs.subtitle.value = subTitle
        vc.viewModel.outputs.desc.value = desc
        vc.viewModel.outputs.actionType.value = .flatButton
        vc.viewModel.outputs.buttonText.value = buttonText
        vc.viewModel.outputs.popUpType.value = .animation
        vc.viewModel.outputs.animationName.value = animationName
        
        vc.viewModel.outputs.okHandler.value = {
            DispatchQueue.main.async { [weak self] in
                guard self != nil else { return }
                vc.dismiss(animated: true, completion: nil)
                
            
            }
        }
        
        self.presentPopUpController(
            tapDismiss: true,
            controller: { () -> UIViewController? in
                return vc
        },
            completion: nil
        )
    }

}

extension HomeViewController {
    // Popup
    func showShortcutPopup() {
        let vcIdentifier = String(describing: GeneralShortcutPopupVC.self)
        guard let vc = UIViewController.vc(
            "GeneralPopup",
            identifier: vcIdentifier
            ) as? GeneralShortcutPopupVC else { return }
        
        vc.viewModel.outputs.titleString.value = "Add Geofence"
        
        vc.viewModel.outputs.leftContainerIconImage.value = UIImage(named: "icon-location")
        vc.viewModel.outputs.leftContainerString.value = "Location"
        
        vc.viewModel.outputs.rightContainerIconImage.value = UIImage(named: "icon-wifi")
        vc.viewModel.outputs.rightContainerString.value = "Wifi"
        vc.delegate = self
        
        presentPopUpController(
            tapDismiss: true,
            popUpType: .Shortcut,
            forBiometric: false,
            customWidth: UIScreen.main.bounds.width,
            style: .bottomSheet,
            controller: { () -> UIViewController? in
                return vc
        },
            completion: nil)
        
    }
}

// MARK: - General Shortcut Popup Delegate
extension HomeViewController: GeneralShortcutPopupVCDelegate {
    func generalShortcutPopupVC(onLeft vc: GeneralShortcutPopupVC) {
        self.dismiss(animated: true, completion: nil)
        
        goToAddLocation()
    }
    
    func generalShortcutPopupVC(onRight vc: GeneralShortcutPopupVC) {
        self.dismiss(animated: true, completion: nil)
        
        let title = "Wifi GeoFence"
        let subTitle = "Add Wifi GeoFence"
        let desc = "This features will be coming on the app soon. Stay tune"
        let buttonText = "Okay"
        let animationName = "coming"
        
        generalPopupImageVC(title: title, subTitle: subTitle, desc: desc, buttonText: buttonText, animationName: animationName)
    }
}

// MARK: - MapView Delegate
extension HomeViewController: MKMapViewDelegate {
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        let identifier = "myGeotification"
        if annotation is Geotification {
            var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier) as? MKPinAnnotationView
            if annotationView == nil {
                annotationView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: identifier)
                annotationView?.canShowCallout = true
                let removeButton = UIButton(type: .custom)
                removeButton.frame = CGRect(x: 0, y: 0, width: 23, height: 23)
                removeButton.setImage(UIImage(named: "DeleteGeotification")!, for: .normal)
                annotationView?.leftCalloutAccessoryView = removeButton
            } else {
                annotationView?.annotation = annotation
            }
            return annotationView
        }
        return nil
    }
    
    
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        if overlay is MKCircle {
            let circleRenderer = MKCircleRenderer(overlay: overlay)
            circleRenderer.lineWidth = 1.0
            circleRenderer.strokeColor = .purple
            circleRenderer.fillColor = UIColor.purple.withAlphaComponent(0.4)
            return circleRenderer
        }
        return MKOverlayRenderer(overlay: overlay)
    }
    
    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {
        // Delete geotification
        let geotification = view.annotation as! Geotification
        remove(geotification)
        saveAllGeotifications()
    }
    
}

// MARK: - Location Manager Delegate
extension HomeViewController: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        mapView.showsUserLocation = status == .authorizedAlways
    }
    
    func locationManager(_ manager: CLLocationManager, monitoringDidFailFor region: CLRegion?, withError error: Error) {
        print("Monitoring failed for region with identifier: \(region!.identifier)")
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Location Manager failed with the following error: \(error)")
    }
    
}

// MARK: AddLocationViewControllerDelegate
extension HomeViewController: AddLocationViewControllerDelegate {
    func addLocationViewController(_ controller: AddLocationViewController, didAddCoordinate coordinate: CLLocationCoordinate2D, radius: Double, identifier: String, note: String, eventType: Geotification.EventType) {
        
        let clampedRadius = min(radius, locationManager.maximumRegionMonitoringDistance)
        let geotification = Geotification(coordinate: coordinate, radius: clampedRadius, identifier: identifier, note: note, eventType: eventType)
        add(geotification)
        startMonitoring(geotification: geotification)
        saveAllGeotifications()

    }
    
}



