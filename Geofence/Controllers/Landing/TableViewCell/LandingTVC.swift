//
//  LandingTVC.swift
//  Geofence
//
//  Created by Wan Rahafiz Wan Abdul Rahim on 23/10/2019.
//  Copyright © 2019 Wan Rahafiz Wan Abdul Rahim. All rights reserved.
//

import UIKit
import Library

protocol LandingTVCDelegate: class {
    func landingTVC(
        _ cell: LandingTVC
    )
}

class LandingTVC: UITableViewCell {

    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var collectionView: UICollectionView!
    
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var csHeightCollectionView: NSLayoutConstraint!
    
    weak var delegate: LandingTVCDelegate?
    let viewModel: LandingTVCViewModelType = LandingTVCViewModel()
    var dataSource = LandingTVCDS()

    override func awakeFromNib() {
        super.awakeFromNib()
        setupView()
        setupListener()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    override var intrinsicContentSize: CGSize {
        let size = containerView.systemLayoutSizeFitting(UIView.layoutFittingCompressedSize)
        
        return size
    }
    
    fileprivate func setupView() {
        selectionStyle = .none
        _ = collectionView.registerCellNibForClass(LandingCVCell.self)
        
        collectionView.delegate = self
        collectionView.dataSource = dataSource
        
        nextButton.layer.cornerRadius = 8
        nextButton.addTarget(self, action: #selector(goToHome), for: .touchUpInside)

        
        configureCardCVCell()
        configurePageControl()
    }
    
    fileprivate func setupListener() {
        
        viewModel.outputs.welcomeCollection.bind { [weak self] value in
            guard let strongSelf = self else { return }
            
            strongSelf.dataSource.set(card: value)
            strongSelf.collectionView.reloadData()
            strongSelf.collectionView.layoutIfNeeded()
        }
        
        viewModel.outputs.currentIndex.bind { [weak self] in
            guard let strongSelf = self else { return }
            
            strongSelf.setPageControlCurrentIndex(index: $0)
            
        }

    }
    
    // MARK: Calculate collection cell size
    fileprivate func configureCardCVCell() {
        let cardCellIdentifier = String(describing: LandingCVCell.self)
        let cardCellNib = UINib.init(nibName: cardCellIdentifier,
                                        bundle: nil)
        let cardCVC = cardCellNib.instantiate(withOwner: self, options: nil).first as? LandingCVCell
        //        let widthCell = viewModel.outputs.fixedItemWidth.value
        
        //cardCVC?.delegate = self
        cardCVC?.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 0)
        
        let cardVM = LandingCVCellViewModel()
        cardVM.outputs.animationName.value = "location"
        cardVM.outputs.description.value = "A device is considered to be inside of the geofence area if the device"
       
        
        cardCVC?.configureWith(value: cardVM)
        cardCVC?.setNeedsDisplay()
        cardCVC?.layoutIfNeeded()
        cardCVC?.layoutSubviews()
        
        let totalWidth =  UIScreen.main.bounds.width - (viewModel.outputs.sectionInset.value.left + viewModel.outputs.sectionInset.value.right)
        let cellWidth = totalWidth
        let cellSize = CGSize.init(width: cellWidth, height: cardCVC?.intrinsicContentSize.height ?? 0)
        csHeightCollectionView.constant = cellSize.height
        
        //TODO: setup snapping flow layout
        setupSnappingCollectionViewLayout(cellSize: cellSize)
    }
    
    fileprivate func setupSnappingCollectionViewLayout(cellSize: CGSize) {
        csHeightCollectionView.constant = cellSize.height + viewModel.outputs.sectionInset.value.top + viewModel.outputs.sectionInset.value.bottom
        
        let snappingFlowLayout = SnappingCollectionViewLayout.init()
        snappingFlowLayout.scrollDirection = .horizontal
        snappingFlowLayout.minimumInteritemSpacing = viewModel.outputs.itemSpacing.value
        snappingFlowLayout.minimumLineSpacing = viewModel.outputs.itemSpacing.value
        snappingFlowLayout.sectionInset = viewModel.outputs.sectionInset.value
        snappingFlowLayout.itemSize = cellSize
        snappingFlowLayout.spacingLeft = 0
        
        collectionView.collectionViewLayout.invalidateLayout()
        collectionView.reloadData()
        collectionView.collectionViewLayout = snappingFlowLayout
        self.layoutIfNeeded()
    }
    
    @objc func goToHome() {
        delegate?.landingTVC(self)
    }

    
}

//MARK: CollectionView Delegate
extension LandingTVC: UICollectionViewDelegate, UICollectionViewDelegateFlowLayout  {
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
     
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        let edgeInsets = viewModel.outputs.sectionInset.value
        
        /*if section == ParkingSessionTVCDS.Section.AddSession.rawValue {
            if let sessions = viewModel.outputs.sessions.value, sessions.count > 0 {
                edgeInsets = UIEdgeInsets(top: edgeInsets.top, left: -16, bottom: edgeInsets.bottom, right: edgeInsets.right)
            } else {
                edgeInsets = UIEdgeInsets(top: edgeInsets.top, left: -16, bottom: edgeInsets.bottom, right: edgeInsets.right)
            }
        }*/
        return edgeInsets
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
    
    // MARK: Detecting Collectionview Scrolling
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let x = collectionView.contentOffset.x
        let w = collectionView.bounds.size.width
        let currentPage = Int(ceil(x/w))
        print("Current Page: \(currentPage)")
        
        viewModel.outputs.currentIndex.value = currentPage
    }
  
    // MARK: Set current page for pagecontrol
    func setPageControlCurrentIndex(index: Int?) {
        guard let index = index else { return }
        pageControl.currentPage = index
    }
    
    // MARK: Configure page control
    func configurePageControl() {
        viewModel.outputs.currentIndex.value = 0
        self.pageControl.isUserInteractionEnabled = false
    }

}

// MARK: View Layout
extension LandingTVC: ValueCell {
    func configureWith(value: LandingTVCViewModelType) {
        viewModel.outputs.welcomeCollection.value = value.outputs.welcomeCollection.value
        viewModel.outputs.itemSpacing.value = value.outputs.itemSpacing.value
        viewModel.outputs.sectionInset.value = value.outputs.sectionInset.value
        viewModel.outputs.currentIndex.value = value.outputs.currentIndex.value

    }
      
}

