//
//  LandingCVCell.swift
//  Geofence
//
//  Created by Wan Rahafiz Wan Abdul Rahim on 23/10/2019.
//  Copyright © 2019 Wan Rahafiz Wan Abdul Rahim. All rights reserved.
//

import UIKit
import Lottie
import Motion

class LandingCVCell: UICollectionViewCell {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var animationView: UIView!
    @IBOutlet weak var descriptionLbl: UILabel!
    
    var animatedView: AnimationView?
    let viewModel: LandingCVCellViewModelType = LandingCVCellViewModel()


    override func awakeFromNib() {
        super.awakeFromNib()
        setupView()
        setupListener()

    }

    override var intrinsicContentSize: CGSize {
        let size = containerView.systemLayoutSizeFitting(UIView.layoutFittingCompressedSize)
        
        return size
    }
    
    func setupView() {
        
    }
    
    func setupListener() {
                
        viewModel.outputs.animationName.bind { [weak self] value in
            guard let strongSelf = self else { return }
            guard let value = value else { return }

            
            strongSelf.setupAnimationView(animationName: value)
        }
        
        viewModel.outputs.description.bind { [weak self] value in
            guard let strongSelf = self else { return }
            guard let value = value else { return }
            
            strongSelf.descriptionLbl.text = value
            
        }
    
       
    }
    
    func setupAnimationView(animationName: String) {
        guard let bundlePath = Bundle.main.path(forResource: animationName, ofType: "bundle") else { return }
        guard let bundle = Bundle(path: bundlePath) else { return }
        
        let _animatedView = AnimationView.init(name: animationName, bundle: bundle)
        
        _animatedView.contentMode = .scaleAspectFit
        _animatedView.frame = animationView.bounds.bounds
        _animatedView.play()
        _animatedView.loopMode = LottieLoopMode.loop
        
        animationView.addSubview(_animatedView)
        
        _animatedView.addBoundedConstraint(
            to: animationView,
            insets: UIEdgeInsets.zero
        )
        
        animatedView = _animatedView
    }
    
}

// MARK: View Layout
extension LandingCVCell: ValueCell {
    func configureWith(value: LandingCVCellViewModelType) {
        viewModel.outputs.animationName.value = value.outputs.animationName.value
        viewModel.outputs.description.value = value.outputs.description.value
    }
    
}

