//
//  LandingViewModel.swift
//  Geofence
//
//  Created by Wan Rahafiz Wan Abdul Rahim on 23/10/2019.
//  Copyright © 2019 Wan Rahafiz Wan Abdul Rahim. All rights reserved.
//

import Foundation

protocol LandingViewModelViewModelInputs {
  
}

protocol LandingViewModelOutputs {
   
    var collection: Box<LandingTVCViewModelType?> { get }
}

protocol LandingViewModelType {
    var inputs: LandingViewModelViewModelInputs { get }
    var outputs: LandingViewModelOutputs { get }
}

class LandingViewModel: LandingViewModelViewModelInputs, LandingViewModelOutputs, LandingViewModelType {
   
    let collection: Box<LandingTVCViewModelType?> = Box(nil)
    
    var inputs: LandingViewModelViewModelInputs { return self }
    var outputs: LandingViewModelOutputs { return self }
    
    init() {
       // Data layout injection 
       let tempWelcomeCollection = LandingTVCViewModel()
       var tempWelcomeCollections = [LandingCVCellViewModel]()
        
        for i in 0..<2 {
            let welcomeCollection = LandingCVCellViewModel()
            if i == 0 {
                
                welcomeCollection.outputs.animationName.value = "technology-application"
                welcomeCollection.outputs.description.value = "Add your Geofence location and track when you enter it"
                
            } else if i == 1 {
                
                welcomeCollection.outputs.animationName.value = "location"
                welcomeCollection.outputs.description.value = "Exit your track Geofence location and get notify"

            }            
            tempWelcomeCollections.append(welcomeCollection)
        }
        
        tempWelcomeCollection.outputs.welcomeCollection.value = tempWelcomeCollections
        collection.value = tempWelcomeCollection
    }
    
}
