//
//  GeneralImagePopupVC.swift
//  Geofence
//
//  Created by Wan Rahafiz Wan Abdul Rahim on 28/10/2019.
//  Copyright © 2019 Wan Rahafiz Wan Abdul Rahim. All rights reserved.
//

import UIKit
import Lottie

protocol GeneralImagePopupVCViewModelInputs {
    func closeButtonOnClick()
    func confirmButtonOnClick()
}

protocol GeneralImagePopupVCViewModelOutputs {
    var popUpType: Box<GeneralImagePopupVCViewModel.PopUpType> { get }
    var actionType: Box<GeneralImagePopupVCViewModel.ActionType> { get }
    var notifyDelegateDidConfirm: Box<Void?> { get }
    var notifyDelegateDidClose: Box<Void?> { get }
    
    var imgUrl: Box<URL?> { get }
    var image: Box<UIImage?> { get }
    var animationName: Box<String?> { get }
    
    var title: Box<String?> { get }
    var titleTextColor: Box<UIColor> { get }
    
    var subtitle: Box<String?> { get }
    var subtitleTextColor: Box<UIColor> { get }
    
    var desc: Box<String?> { get }
    
    var buttonText: Box<String> { get }
    var cancelHandler: Box<(() -> Void)?> { get }
    var okHandler: Box<(() -> Void)?> { get }
    
    var isShowCloseBtn: Box<Bool> { get }
}

protocol GeneralImagePopupVCViewModelType {
    var inputs: GeneralImagePopupVCViewModelInputs { get }
    var outputs: GeneralImagePopupVCViewModelOutputs { get }
}

class GeneralImagePopupVCViewModel: GeneralImagePopupVCViewModelInputs, GeneralImagePopupVCViewModelOutputs, GeneralImagePopupVCViewModelType {
    
    enum PopUpType: Int {
        case image = 1
        case animation = 2
    }
    
    enum ActionType: Int {
        case okButton = 1
        case flatButton = 2
    }
    
    
    let popUpType: Box<GeneralImagePopupVCViewModel.PopUpType> = Box(.image)
    let actionType: Box<GeneralImagePopupVCViewModel.ActionType> = Box(.flatButton)
    let notifyDelegateDidConfirm: Box<Void?> = Box(nil)
    let notifyDelegateDidClose: Box<Void?> = Box(nil)
    
    let imgUrl: Box<URL?> = Box(nil)
    let image: Box<UIImage?> = Box(nil)
    let animationName: Box<String?> = Box(nil)
    
    let title: Box<String?> = Box(nil)
    let titleTextColor: Box<UIColor> = Box(UIColor.black)
    
    let subtitle: Box<String?> = Box(nil)
    let subtitleTextColor: Box<UIColor> = Box(UIColor.contentBlack())
    
    let desc: Box<String?> = Box(nil)
    
    let buttonText: Box<String> = Box("Got it")
    let cancelHandler: Box<(() -> Void)?> = Box(nil)
    let okHandler: Box<(() -> Void)?> = Box(nil)
    
    let isShowCloseBtn: Box<Bool> = Box(true)
    
    func closeButtonOnClick() {
        outputs.notifyDelegateDidClose.value = ()
    }
    
    func confirmButtonOnClick() {
        outputs.notifyDelegateDidConfirm.value = ()
    }
    
    var inputs: GeneralImagePopupVCViewModelInputs { return self }
    var outputs: GeneralImagePopupVCViewModelOutputs { return self }
}

protocol GeneralImagePopUpVCDelegate: class {
    func generalImagePopUpVCDidClose(
        _ vc: GeneralImagePopupVC
    )
    
    func generalImagePopUpDidConfirm(
        _ vc: GeneralImagePopupVC
    )
}

class GeneralImagePopupVC: UIViewController {
    
    @IBOutlet weak var animationContainerView: UIView!
    @IBOutlet weak var closeButton: UIButton!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var subtitleLbl: UILabel!
    @IBOutlet weak var descLbl: UILabel!
    @IBOutlet weak var confirmButton: UIButton!
    @IBOutlet weak var okButton: UIButton!
    
    var animatedView: AnimationView?
    
    let viewModel: GeneralImagePopupVCViewModelType = GeneralImagePopupVCViewModel()
    weak var delegate: GeneralImagePopUpVCDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        setupListener()
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    fileprivate func setupView() {
        confirmButton.layer.cornerRadius = confirmButton.frame.height / 2
        okButton.clipsToBounds = true
        okButton.layer.cornerRadius = okButton.frame.height / 2.0
        okButton.backgroundColor = UIColor.clear
        okButton.setImage(UIImage(named: "tick-white")?
            .withRenderingMode(.alwaysOriginal),
                          for: .normal)
    }
    
    @IBAction func closeButtonOnClick() {
        viewModel.inputs.closeButtonOnClick()
    }
    
    @IBAction func confirmButtonOnClick() {
        viewModel.inputs.confirmButtonOnClick()
    }
    
    fileprivate func setupListener() {
        okButton.addTarget(self, action: #selector(confirmButtonOnClick), for: .touchUpInside)
        
        viewModel.outputs.popUpType.bind { [weak self] result in
            guard let strongSelf = self else { return }
            
            switch result {
            case .image:
                strongSelf.animationContainerView.alpha = 0
                strongSelf.imgView.alpha = 1
                break
            case .animation:
                strongSelf.animationContainerView.alpha = 1
                strongSelf.imgView.alpha = 0
                break
            }
        }
        
        viewModel.outputs.notifyDelegateDidConfirm.bind { [weak self] in
            guard let strongSelf = self else { return }
            guard $0 != nil else { return }
            
            if let okHandler = strongSelf.viewModel.outputs.okHandler.value {
                okHandler()
            } else {
                strongSelf.delegate?.generalImagePopUpDidConfirm(strongSelf)
            }
            
            strongSelf.viewModel.outputs.notifyDelegateDidConfirm.value = nil
        }
        
        viewModel.outputs.notifyDelegateDidClose.bind { [weak self] in
            guard let strongSelf = self else { return }
            guard $0 != nil else { return }
            
            if let cancelHandler = strongSelf.viewModel.outputs.cancelHandler.value {
                cancelHandler()
            } else {
                strongSelf.delegate?.generalImagePopUpVCDidClose(strongSelf)
            }
            
            strongSelf.viewModel.outputs.notifyDelegateDidClose.value = nil
        }
        
        viewModel.outputs.animationName.bind { [weak self] in
            guard let strongSelf = self else { return }
            guard let animationName = $0 else { return }
            
            strongSelf.setupAnimationView(animationName: animationName)
        }
        
        viewModel.outputs.title.bind { [weak self] in
            guard let strongSelf = self else { return }
            
            strongSelf.titleLbl.text = $0
        }
        
        
        viewModel.outputs.titleTextColor.bind { [weak self] in
            guard let strongSelf = self else { return }
            
            strongSelf.titleLbl.textColor = $0
        }
        
        viewModel.outputs.subtitle.bind { [weak self] in
            guard let strongSelf = self else { return }
            
            strongSelf.subtitleLbl.text = $0
        }
        
     
        
        viewModel.outputs.subtitleTextColor.bind { [weak self] in
            guard let strongSelf = self else { return }
            
            strongSelf.subtitleLbl.textColor = $0
        }
        
        viewModel.outputs.desc.bind { [weak self] in
            guard let strongSelf = self else { return }
            guard let descStr = $0 else { return }
            //            strongSelf.descLbl.text = $0
            strongSelf.descLbl.attributedText = ThemeManager.htmlAttributedString(text: descStr, regularFont: UIFont(name: "futura", size: 14) ?? UIFont.systemFont(ofSize: 14), boldFont: UIFont(name: "Futura-bold", size: 14) ?? UIFont.boldSystemFont(ofSize: 14), textColor: UIColor.contentBlack())
            
        }
        
        viewModel.outputs.buttonText.bind { [weak self] in
            guard let strongSelf = self else { return }
            
            strongSelf.confirmButton.setTitle($0, for: .normal)
        }
        
        viewModel.outputs.image.bind { [weak self ] img in
            guard let strongSelf = self else { return }
            
            strongSelf.imgView.image = img
        }
        
        viewModel.outputs.actionType.bind { [weak self ] actionType in
            guard let strongSelf = self else { return }
            
            if actionType == .flatButton {
                strongSelf.okButton.isEnabled = false
                strongSelf.okButton.isHidden = true
            } else {
                strongSelf.confirmButton.isEnabled = false
                strongSelf.confirmButton.isHidden = true
            }
        }
        
        viewModel.outputs.isShowCloseBtn.bind { [weak self] flag in
            guard let strongSelf = self else { return }
            
            strongSelf.closeButton.isHidden = !flag
            
        }
    }
    
    func setupAnimationView(animationName: String) {
        //        guard let animationTitle = animationName else { return }
        guard let bundlePath = Bundle.main.path(forResource: animationName, ofType: "bundle") else { return }
        guard let bundle = Bundle(path: bundlePath) else { return }
        
        let _animatedView = AnimationView.init(name: animationName, bundle: bundle)
        
        _animatedView.contentMode = .scaleAspectFit
        _animatedView.frame = animationContainerView.bounds.bounds
        _animatedView.play()
        _animatedView.loopMode = LottieLoopMode.loop
        
        animationContainerView.addSubview(_animatedView)
        
        _animatedView.addBoundedConstraint(
                  to: animationContainerView,
                  insets: UIEdgeInsets.zero
        )
        
        animatedView = _animatedView
    }
    
}

