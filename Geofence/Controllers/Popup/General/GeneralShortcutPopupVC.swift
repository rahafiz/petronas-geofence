//
//  GeneralShortcutPopupVC.swift
//  Geofence
//
//  Created by Wan Rahafiz Wan Abdul Rahim on 26/10/2019.
//  Copyright © 2019 Wan Rahafiz Wan Abdul Rahim. All rights reserved.
//

import UIKit

protocol GeneralShortcutPopupVCViewModelInputs {
    func leftContainerOnClick()
    func rightContainerOnClick()
}

protocol GeneralShortcutPopupVCViewModelOutputs {
    var titleString: Box<String?> { get }
    var leftContainerString: Box<String?> { get }
    var rightContainerString: Box<String?> { get }
    var leftContainerIconImage: Box<UIImage?> { get }
    var rightContainerIconImage: Box<UIImage?> { get }
    
    var notifyDelegateLeftContainerOnClick: Box<Void?> { get }
    var notifyDelegateRightContainerOnClick: Box<Void?> { get }
}

protocol GeneralShortcutPopupVCViewModelType {
    var inputs: GeneralShortcutPopupVCViewModelInputs { get }
    var outputs: GeneralShortcutPopupVCViewModelOutputs { get }
}

protocol GeneralShortcutPopupVCDelegate: class {
    func generalShortcutPopupVC(
        onLeft vc: GeneralShortcutPopupVC
    )
    
    func generalShortcutPopupVC(
        onRight vc: GeneralShortcutPopupVC
    )
}

class GeneralShortcutPopupVCViewModel: GeneralShortcutPopupVCViewModelType, GeneralShortcutPopupVCViewModelInputs, GeneralShortcutPopupVCViewModelOutputs {
    let titleString: Box<String?> = Box(nil)
    let leftContainerString: Box<String?> = Box(nil)
    let rightContainerString: Box<String?> = Box(nil)
    let leftContainerIconImage: Box<UIImage?> = Box(nil)
    let rightContainerIconImage: Box<UIImage?> = Box(nil)
    let notifyDelegateLeftContainerOnClick: Box<Void?> = Box(nil)
    let notifyDelegateRightContainerOnClick: Box<Void?> = Box(nil)
    
    var inputs: GeneralShortcutPopupVCViewModelInputs { return self }
    var outputs: GeneralShortcutPopupVCViewModelOutputs { return self }
    
    func leftContainerOnClick() {
        outputs.notifyDelegateLeftContainerOnClick.value = ()
    }
    
    func rightContainerOnClick() {
        outputs.notifyDelegateRightContainerOnClick.value = ()
    }
}

class GeneralShortcutPopupVC: UIViewController {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var leftContainerImageView: UIImageView!
    @IBOutlet weak var rightContainerImageView: UIImageView!
    @IBOutlet weak var leftContainerLabel: UILabel!
    @IBOutlet weak var rightContainerLabel: UILabel!
    @IBOutlet weak var leftContainerButton: UIButton!
    @IBOutlet weak var rightContainerButton: UIButton!
    
    let viewModel: GeneralShortcutPopupVCViewModelType = GeneralShortcutPopupVCViewModel()
    weak var delegate: GeneralShortcutPopupVCDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        setupView()
        setupListener()
    }
    
    func setupView() {
    }
    
    func setupListener() {
        leftContainerButton.addTarget(self, action: #selector(onLeftContainerPressed), for: .touchUpInside)
        rightContainerButton.addTarget(self, action: #selector(onRightContainerPressed), for: .touchUpInside)
        
        viewModel.outputs.titleString.bind { [weak self] in
            guard let strongSelf = self else { return }
            guard let value = $0 else { return }
            
            strongSelf.titleLabel.text = value
        }
        
        viewModel.outputs.leftContainerString.bind { [weak self] in
            guard let strongSelf = self else { return }
            guard let value = $0 else { return }
            
            strongSelf.leftContainerLabel.text = value
        }
        
        viewModel.outputs.leftContainerIconImage.bind { [weak self] in
            guard let strongSelf = self else { return }
            guard let value = $0 else { return }
            
            strongSelf.leftContainerImageView.image = value
        }
        
        viewModel.outputs.notifyDelegateLeftContainerOnClick.bind { [weak self] in
            guard let strongSelf = self else { return }
            guard let _ = $0 else { return }
            
            strongSelf.delegate?.generalShortcutPopupVC(onLeft: strongSelf)
        }
        
        viewModel.outputs.rightContainerString.bind { [weak self] in
            guard let strongSelf = self else { return }
            guard let value = $0 else { return }
            
            strongSelf.rightContainerLabel.text = value
        }
        
        viewModel.outputs.rightContainerIconImage.bind { [weak self] in
            guard let strongSelf = self else { return }
            guard let value = $0 else { return }
            
            strongSelf.rightContainerImageView.image = value
        }
        
        viewModel.outputs.notifyDelegateRightContainerOnClick.bind { [weak self] in
            guard let strongSelf = self else { return }
            guard let _ = $0 else { return }
            
            strongSelf.delegate?.generalShortcutPopupVC(onRight: strongSelf)
            strongSelf.viewModel.outputs.notifyDelegateRightContainerOnClick.value = nil
        }
    }
    
    @objc func onLeftContainerPressed() {
        viewModel.inputs.leftContainerOnClick()
    }
    
    @objc func onRightContainerPressed() {
        viewModel.inputs.rightContainerOnClick()
    }
}
