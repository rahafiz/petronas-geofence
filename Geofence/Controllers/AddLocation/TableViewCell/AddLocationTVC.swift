//
//  AddLocationTVC.swift
//  Geofence
//
//  Created by Wan Rahafiz Wan Abdul Rahim on 28/10/2019.
//  Copyright © 2019 Wan Rahafiz Wan Abdul Rahim. All rights reserved.
//

import UIKit

protocol AddLocationTVCDelegate: class {
    func AddLocationTVC(
        _ cell: LandingTVC
    )
}

class AddLocationTVC: UITableViewCell {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var indicatorLabel: UILabel!
    
    let viewModel: AddLocationTVCViewModelType = AddLocationTVCViewModel()

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setupView()
        setupListener()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    fileprivate func setupView() {
    
    }
    
    fileprivate func setupListener() {
        
        viewModel.outputs.title.bind { [weak self] value in
            guard let strongSelf = self else { return }
            guard let value = value else { return }
            
            strongSelf.locationLabel.text = value
        }
        
        viewModel.outputs.indicator.bind { [weak self] value in
            guard let strongSelf = self else { return }
            guard let value = value else { return }
            
            strongSelf.indicatorLabel.text = value
        }

        
    }
    
}

// MARK: View Layout
extension AddLocationTVC: ValueCell {
    func configureWith(value: AddLocationTVCViewModelType) {
        viewModel.outputs.title.value = value.outputs.title.value
        viewModel.outputs.indicator.value = value.outputs.indicator.value
        viewModel.outputs.indicatorFlag.value = value.outputs.indicatorFlag.value
        viewModel.outputs.latitude.value = value.outputs.latitude.value
        viewModel.outputs.longitude.value = value.outputs.longitude.value
    }
    
}

