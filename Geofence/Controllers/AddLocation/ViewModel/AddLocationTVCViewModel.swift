//
//  AddLocationTVCViewModel.swift
//  Geofence
//
//  Created by Wan Rahafiz Wan Abdul Rahim on 28/10/2019.
//  Copyright © 2019 Wan Rahafiz Wan Abdul Rahim. All rights reserved.
//

import Foundation
import CoreLocation

protocol AddLocationTVCViewModelInputs {
    
}

protocol AddLocationTVCViewModelOutputs {
    
    var title: Box<String?> { get }
    var indicator: Box<String?> { get }
    var indicatorFlag: Box<Bool> { get }
    var latitude: Box<CLLocationDegrees?> { get }
    var longitude: Box<CLLocationDegrees?> { get }
    
}

protocol AddLocationTVCViewModelType {
    var inputs: AddLocationTVCViewModelInputs { get }
    var outputs: AddLocationTVCViewModelOutputs { get }
}

class AddLocationTVCViewModel: AddLocationTVCViewModelInputs, AddLocationTVCViewModelOutputs, AddLocationTVCViewModelType {
    
    
    let title: Box<String?> = Box(nil)
    let indicator: Box<String?> = Box(nil)
    let indicatorFlag: Box<Bool> = Box(true)
    let latitude: Box<CLLocationDegrees?> = Box(nil)
    let longitude: Box<CLLocationDegrees?> = Box(nil)
    
    var inputs: AddLocationTVCViewModelInputs { return self }
    var outputs: AddLocationTVCViewModelOutputs { return self }
    
    init() {
        
    }
    
}
