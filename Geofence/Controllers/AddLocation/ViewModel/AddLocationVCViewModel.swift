//
//  AddLocationVCViewModel.swift
//  Geofence
//
//  Created by Wan Rahafiz Wan Abdul Rahim on 28/10/2019.
//  Copyright © 2019 Wan Rahafiz Wan Abdul Rahim. All rights reserved.
//

import Foundation

protocol AddLocationVCViewModelInputs {
    
}

protocol AddLocationVCViewModelOutputs {
    
    var location: Box<[AddLocationTVCViewModelType]?> { get }
    
}

protocol AddLocationVCViewModelType {
    var inputs: AddLocationVCViewModelInputs { get }
    var outputs: AddLocationVCViewModelOutputs { get }
}

class AddLocationVCViewModel: AddLocationVCViewModelInputs, AddLocationVCViewModelOutputs, AddLocationVCViewModelType {
    
    let location: Box<[AddLocationTVCViewModelType]?> = Box(nil)
    
    var inputs: AddLocationVCViewModelInputs { return self }
    var outputs: AddLocationVCViewModelOutputs { return self }
    
    init() {
        // Data layout injection
        var tempLocations = [AddLocationTVCViewModel]()
        for i in 0..<8 {
            let location = AddLocationTVCViewModel()
            if i == 0 {
               location.outputs.title.value = "Petronas, Ampang Jaya"
               location.outputs.indicator.value = "Enter"
               location.outputs.indicatorFlag.value = true
               location.outputs.latitude.value = 3.160218
               location.outputs.longitude.value = 101.754379
            }
            if i == 1 {
                location.outputs.title.value = "Petronas, Ampang Jaya"
                location.outputs.indicator.value = "Exit"
                location.outputs.indicatorFlag.value = false
                location.outputs.latitude.value = 3.160218
                location.outputs.longitude.value = 101.754379
            }
            else if i == 2 {
                location.outputs.title.value = "Petronas, MRR2 Ampang (Tesco)"
                location.outputs.indicator.value = "Enter"
                location.outputs.indicatorFlag.value = true
                location.outputs.latitude.value = 3.141534
                location.outputs.longitude.value = 101.742467
            }
            else if i == 3 {
                location.outputs.title.value = "Petronas, MRR2 Ampang (Tesco)"
                location.outputs.indicator.value = "Exit"
                location.outputs.indicatorFlag.value = false
                location.outputs.latitude.value = 3.141534
                location.outputs.longitude.value = 101.742467

            }
            else if i == 4 {
                location.outputs.title.value = "Petronas, Taman Cempaka Ampang"
                location.outputs.indicator.value = "Enter"
                location.outputs.indicatorFlag.value = true
                location.outputs.latitude.value = 3.139742
                location.outputs.longitude.value = 101.751814
            }
            else if i == 5 {
                location.outputs.title.value = "Petronas, Taman Cempaka Ampang"
                location.outputs.indicator.value = "Exit"
                location.outputs.indicatorFlag.value = false
                location.outputs.latitude.value = 3.139742
                location.outputs.longitude.value = 101.751814
            }
            else if i == 6 {
                location.outputs.title.value = "Petronas, Prima Saujana Kajang"
                location.outputs.indicator.value = "Enter"
                location.outputs.indicatorFlag.value = true
                location.outputs.latitude.value = 3.008173
                location.outputs.longitude.value = 101.803485
            }
            else if i == 7 {
                location.outputs.title.value = "Petronas, Prima Saujana Kajang"
                location.outputs.indicator.value = "Exit"
                location.outputs.indicatorFlag.value = false
                location.outputs.latitude.value = 3.008173
                location.outputs.longitude.value = 101.803485
            }

            tempLocations.append(location)

        }
        
       location.value = tempLocations
    }
    
}
