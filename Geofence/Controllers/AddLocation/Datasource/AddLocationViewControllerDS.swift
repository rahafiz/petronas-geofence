//
//  AddLocationViewControllerDS.swift
//  Geofence
//
//  Created by Wan Rahafiz Wan Abdul Rahim on 28/10/2019.
//  Copyright © 2019 Wan Rahafiz Wan Abdul Rahim. All rights reserved.
//

import Foundation
import Library
import UIKit


class AddLocationViewControllerDS: ValueCellDataSource {
    enum Section: Int {
        case Location
        
    }
    
    override init() {
        super.init()
    }
    
    
    func set(location: [AddLocationTVCViewModelType]?) {
        let section = Section.Location.rawValue
        
        self.clearValues(section: section)
        
        if let location = location {
            self.set(
                values: location,
                cellClass: AddLocationTVC.self,
                inSection: section
            )
        }
        
    }
    
    override func configureCell(tableCell cell: UITableViewCell, withValue value: Any) {
        switch (cell, value) {
        case let (cell as AddLocationTVC, value as AddLocationTVCViewModelType):
            cell.configureWith(value: value)
        default:
            assertionFailure("Unrecognized combo: \(cell), \(value)")
        }
    }
}
