//
//  AddLocationViewController.swift
//  Geofence
//
//  Created by Wan Rahafiz Wan Abdul Rahim on 28/10/2019.
//  Copyright © 2019 Wan Rahafiz Wan Abdul Rahim. All rights reserved.
//

import UIKit
import CoreLocation

protocol AddLocationViewControllerDelegate {
    func addLocationViewController(_ controller: AddLocationViewController, didAddCoordinate coordinate: CLLocationCoordinate2D, radius: Double, identifier: String, note: String, eventType: Geotification.EventType)
}

class AddLocationViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var backButton: UIButton!
    
    var delegate: AddLocationViewControllerDelegate?

    let viewModel: AddLocationVCViewModelType = AddLocationVCViewModel()
    var dataSource = AddLocationViewControllerDS()
    
    fileprivate var cellHeightsDict = [IndexPath: CGFloat]()

    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        setupListener()
        setupNavBar()


    }
    
    private func setupView() {
        
        tableView.estimatedRowHeight = UITableView.automaticDimension
        tableView.rowHeight = UITableView.automaticDimension
        tableView.separatorStyle = .none
        
    }
    
    private func setupNavBar() {
        
        backButton.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        backButton.addTarget(self, action: #selector(goBack), for: .touchUpInside)
        backButton.setImage(UIImage(named: "icon-back-black"), for: .normal)
        backButton.backgroundColor = UIColor.clear
    }
    
    
    func setupListener() {
        
        tableView.registerCellNibForClass(AddLocationTVC.self)
        tableView.dataSource = dataSource
        tableView.delegate = self
        
        
        viewModel.outputs.location.bind { [weak self] value in
            guard let strongSelf = self else { return }
            guard let value = value else { return }
            
            DispatchQueue.main.async {
                strongSelf.dataSource.set(location: value)
                strongSelf.tableView.reloadData()
                strongSelf.tableView.layoutIfNeeded()
            }
        }
    }
    
    @objc func goBack() {
        navigationController?.popViewController(animated: true)
    }
    
}

//MARK:Tableview Cell Delegate
extension AddLocationViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        if let height = cellHeightsDict[indexPath] {
            return height
        }
        
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cellHeightsDict[indexPath] = cell.frame.height
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.section == AddLocationViewControllerDS.Section.Location.rawValue {
            guard let selectedLocation = viewModel.outputs.location.value?[indexPath.row] else { return }
            
            guard let selectedLocationLatitude = selectedLocation.outputs.latitude.value else { return }
            guard let selectedLocationLongitude = selectedLocation.outputs.longitude.value else { return }
            guard let selectedLocationTitle = selectedLocation.outputs.title.value else { return }

            let coordinate = CLLocationCoordinate2DMake(selectedLocationLatitude, selectedLocationLongitude)
            let identifier = selectedLocationTitle
            var eventType: Geotification.EventType = .onEntry
            var note = "Welcome to \(identifier)"
            if selectedLocation.outputs.indicatorFlag.value == false {
                eventType = .onExit
                note = "Goodbye \(identifier), until we meet again"
            }
            goBack()
            delegate?.addLocationViewController(self, didAddCoordinate: coordinate, radius: 10, identifier: identifier, note: note , eventType: eventType)


        }

    }
}

