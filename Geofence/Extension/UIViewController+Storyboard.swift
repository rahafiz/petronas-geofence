//
//  UIViewController+Storyboard.swift
//  Geofence
//
//  Created by Wan Rahafiz Wan Abdul Rahim on 25/10/2019.
//  Copyright © 2019 Wan Rahafiz Wan Abdul Rahim. All rights reserved.
//

import UIKit
import Lottie
import SVProgressHUD
import STPopup

protocol VCPageViewModelInputs {
    
}

protocol VCPageViewModelOutputs {
    var pageId: Box<String?> { get }
    var pageName: Box<String?> { get }
}

protocol VCPageViewModelType {
    var inputs: VCPageViewModelInputs { get }
    var outputs: VCPageViewModelOutputs { get }
}

class VCPageViewModel: VCPageViewModelInputs, VCPageViewModelOutputs, VCPageViewModelType {
    enum GeneralPopUpHeight: CGFloat {
        case Biometric = 330
        case Shortcut = 200
    }
    let pageId: Box<String?> = Box(nil)
    let pageName: Box<String?> = Box(nil)
    
    var inputs: VCPageViewModelInputs { return self }
    var outputs: VCPageViewModelOutputs { return self }
}

extension UITableViewCell {
   
}

extension UIViewController {
    
    
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    static func vc<T>(_ storyboard: String, ofType type: T.Type) -> T? where T: UIViewController {
        return vc(storyboard, identifier: String(describing: type)) as? T
    }
    
    static func vc(_ storyboard: String, identifier: String?) -> UIViewController {
        let storyBoardInstance = UIStoryboard(name: storyboard, bundle: nil)
        
        if let _identifier = identifier {
            return storyBoardInstance.instantiateViewController(
                withIdentifier: _identifier
            )
        } else {
            return storyBoardInstance.instantiateInitialViewController()!
        }
    }
    
    func vc(_ storyboard: String? = nil, identifier: String? = nil) -> UIViewController {
        var _storyboard = self.storyboard!
        if storyboard != nil {
            _storyboard = UIStoryboard(name: storyboard!, bundle: nil)
        }
        
        if identifier != nil {
            return _storyboard.instantiateViewController(withIdentifier: identifier!)
        } else {
            return _storyboard.instantiateInitialViewController()!
        }
    }
    
    func addBackButtonInNavItem(barStyle: UIStatusBarStyle = .default,
                                action : ( () -> Void )? = nil) {
        self.actionHandleBlock(action: action)
        
        let img: UIImage? = {
            switch barStyle {
            case .lightContent:
                return UIImage(named: "back-white")?
                    .withRenderingMode(.alwaysOriginal)
            case .default:
                return UIImage(named: "back")?
                    .withRenderingMode(.alwaysOriginal)
            case .blackOpaque:
                return UIImage(named: "back-white")?
                    .withRenderingMode(.alwaysOriginal)
            case .darkContent:
                return UIImage(named: "back-white")?
                .withRenderingMode(.alwaysOriginal)
            }
        }()
        let backBtn = UIBarButtonItem(
            image: img,
            style: .plain,
            target: self,
            action: #selector(pop)
        )
        //        backBtn.imageInsets = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
        self.navigationItem.leftBarButtonItem = backBtn
    }
    
    private func actionHandleBlock(action:(() -> Void)? = nil) {
        struct __ {
            static var actionList: [UIViewController : (() -> Void)] = [:]
        }
        
        if action != nil {
            __.actionList[self] =  action
        } else {
            //when button pressed , view will unload.
            if __.actionList[self] != nil {
                __.actionList[self]!()
                __.actionList.removeValue(forKey: self)
            }
        }
    }
    
    @objc private func pop() {
        self.actionHandleBlock()
        
        if self.navigationController?.viewControllers.count ?? 0 > 1 {
            self.navigationController?.popViewController(animated: true)
        } else {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    func presentPopUpController(tapDismiss: Bool = false,
                                popUpType: VCPageViewModel.GeneralPopUpHeight? = nil,
                                forBiometric: Bool = false,
                                customHeight: CGFloat = 460.0,
                                customWidth: CGFloat = UIScreen.main.bounds.width - 48,
                                cornerRadius: CGFloat = 16,
                                style: STPopupStyle = STPopupStyle.formSheet,
                                controller: () -> UIViewController?,
                                completion: (() -> Void)? = nil
        ) {
        guard let vc = controller() else {
            //BSTLogger.shared.debug("presentPopUpController did not receive a valid UIViewController")
            return
        }
        
        var cHeight = customHeight
        var cWidth = customWidth
        //        var cCornerRadius = cornerRadius
        var cStyle = style
        
        if forBiometric {
            cHeight = 330
            cWidth = UIScreen.main.bounds.width
            //            cCornerRadius = 16
            cStyle = STPopupStyle.bottomSheet
        }
        
        if let popUpType = popUpType {
            cHeight = popUpType.rawValue
        }
        
        if vc.contentSizeInPopup == CGSize.zero {
            //BSTLogger.shared.debug("The height of the overlay = \(cHeight)")
            //                let size = UIScreen.main.bounds
            vc.contentSizeInPopup = CGSize(width: cWidth,
                                           height: cHeight)
        }
        
        let popUpController = STPopupController(rootViewController: vc)
        
        if tapDismiss {
            let tapBackground = UITapGestureRecognizer(target: popUpController, action: #selector(popUpController.dismiss))
            popUpController.backgroundView?.addGestureRecognizer(tapBackground)
        }
        
        popUpController.containerView.layer.cornerRadius = cornerRadius
        popUpController.style = cStyle
        popUpController.navigationBarHidden = true
        popUpController.backgroundView?.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        popUpController.present(in: self as UIViewController,
                                completion: completion)
    }
    
    func handleBasicExceedLimitError() {
        navigationController?.pushViewController(UIStoryboard(name: "UpgradePremium", bundle: nil).instantiateInitialViewController()!, animated: true)
    }
    
    func bstGoBack(animated: Bool) {
        if navigationController != nil {
            navigationController?.popViewController(animated: animated)
        } else {
            dismiss(animated: true,
                    completion: nil)
        }
    }
    
    func navigateToScreen(identifier: String, storyBoardName: String, withPush: Bool = true) {
        let vc = UIViewController.vc(storyBoardName, identifier: identifier)
        if withPush {
            navigationController?.pushViewController(vc, animated: true)
        } else {
            present(vc, animated: true, completion: nil)
        }
    }
    
    
    // To support passing bstGoBack method as selector
    @objc func bstGoBack(_ sender: AnyObject?) {
        bstGoBack(animated: true)
    }
    
    func bstGoBack() {
        bstGoBack(animated: true)
    }
    
    func popOutActivityView(_ message: String) {
        let shareItems: Array = [ message] as [Any]
        
        let activityViewController = UIActivityViewController(
            activityItems: shareItems,
            applicationActivities: nil
        )
        activityViewController.excludedActivityTypes = [
            UIActivity.ActivityType.print, UIActivity.ActivityType.postToWeibo,
            UIActivity.ActivityType.postToVimeo, UIActivity.ActivityType.saveToCameraRoll
        ]
        activityViewController.popoverPresentationController?.sourceView = self.view
        
        present(activityViewController, animated: true, completion: nil)
    }
    
    func UIColorFromRGB(rgbValue: UInt) -> UIColor {
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    
}
