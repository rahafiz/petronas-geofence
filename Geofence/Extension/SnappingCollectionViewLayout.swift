//
//  SnappingCollectionViewLayout.swift
//  Geofence
//
//  Created by Wan Rahafiz Wan Abdul Rahim on 24/10/2019.
//  Copyright © 2019 Wan Rahafiz Wan Abdul Rahim. All rights reserved.
//

import UIKit

class SnappingCollectionViewLayout: UICollectionViewFlowLayout {
    
    var spacingLeft: CGFloat = 8.0
    var centerCellFlag: Bool = false
    
    private func pageWidth() -> CGFloat {
        return self.itemSize.width + self.minimumLineSpacing
    }
    
    private func flickVelocity() -> CGFloat {
        return 0.4
    }
    
    override func targetContentOffset(forProposedContentOffset proposedContentOffset: CGPoint, withScrollingVelocity velocity: CGPoint) -> CGPoint {
        let cvOffsetX = self.collectionView?.contentOffset.x ?? 0
        
        let rawPageValue = cvOffsetX / self.pageWidth()
        let currentPage = (velocity.x > 0.0) ? floor(rawPageValue) : ceil(rawPageValue)
        let nextPage = (velocity.x > 0.0) ? ceil(rawPageValue) : floor(rawPageValue)
        
        let pannedLessThanAPage = fabs(1 + currentPage - rawPageValue) > 0.5
        let flicked = fabs(velocity.x) > self.flickVelocity()
        
        var tempProposedContentOffset = proposedContentOffset
        
        if pannedLessThanAPage && flicked {
            tempProposedContentOffset.x = nextPage * self.pageWidth()
        } else {
            tempProposedContentOffset.x = round(rawPageValue) * self.pageWidth()
        }
        
        if centerCellFlag {
            let cvWidth = self.collectionView?.frame.width ?? 0
            
            let tempSpacingLeft = (cvWidth - self.itemSize.width - self.sectionInset.left - self.sectionInset.right) / 2
            if tempSpacingLeft > 0 {
                spacingLeft = tempSpacingLeft
            }
        }
        
        tempProposedContentOffset.x = tempProposedContentOffset.x - spacingLeft
        
        return tempProposedContentOffset
    }
    
}

