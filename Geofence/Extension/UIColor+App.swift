//
//  UIColor+App.swift
//  Geofence
//
//  Created by Wan Rahafiz Wan Abdul Rahim on 28/10/2019.
//  Copyright © 2019 Wan Rahafiz Wan Abdul Rahim. All rights reserved.
//

import Foundation
import UIKit
import UIColor_Hex_Swift

extension UIColor {
    static func primaryRed() -> UIColor {
        return UIColor("#F03D3D")
    }
    
    static func superShakeRed() -> UIColor {
        return UIColor("#ee2e24")
    }
    
    static func alternativeRed() -> UIColor {
        return UIColor("#ED3124")
    }
    
    static func primaryOrange() -> UIColor {
        return UIColor("#F5A623")
    }
    
    static func boostGreen() -> UIColor {
        return UIColor("#159191")
    }
    
    static func boostGreenRaya() -> UIColor {
        return UIColor("#009f76")
    }
    
    static func boostRedChristmas() -> UIColor {
        return UIColor("#c31717")
    }
    
    static func boostYellow() -> UIColor {
        return UIColor("#FAC934")
    }
    
    static func boostGray() -> UIColor {
        return UIColor("#58595B")
    }
    
    static func boostLightGray() -> UIColor {
        return UIColor("#cdcdcd")
    }
    
    static func boostBackgroundGray() -> UIColor {
        return UIColor("#f3f4f4")
    }
    
    static func boostBackgroundLightGray() -> UIColor {
        return UIColor("#f9f9f9")
    }
    
    static func titleBlack() -> UIColor! {
        return UIColor("#231F20")
    }
    
    static func placeholderDefault() -> UIColor! {
        return UIColor("#8A8A8C")
    }
    
    static func contentBlack() -> UIColor! {
        return UIColor("#58595b")
    }
    
    static func alternativeBlack() -> UIColor! {
        return UIColor("#3a3a3a")
    }
    
    static func captionBlack() -> UIColor! {
        return UIColor("#616063")
    }
    
    static func infoBlack() -> UIColor! {
        return UIColor("#3c4040")
    }
    
    static func receiptGreen() -> UIColor! {
        return UIColor("#479fa1")
    }
    
    static func placeholderColor() -> UIColor! {
        return UIColor("#8a8a8c")
    }
    
    static func actionLightGrayColor() -> UIColor! {
        return UIColor("#9b9b9b")
    }
    
    static func borderGrayColor() -> UIColor! {
        return UIColor("#f0f0f0")
    }
    
    static func backgroundGrayColor() -> UIColor! {
        return UIColor("#f7f7f7")
    }
    
    static func customBlackFontColor() -> UIColor {
        return UIColor("#232323")
    }
    
    static func descriptionGrayColor() -> UIColor {
        return UIColor("#4c4c4c")
    }
    
    static func scoreBoardYellowColor() -> UIColor {
        return UIColor("#fdca2f")
    }
    
    static func scoreBoardRedColor() -> UIColor {
        return UIColor("#d72a2a")
    }
    
    static func unselectedGray() -> UIColor {
        return UIColor("#aeaeae")
    }
    
    static func searchTextBlack() -> UIColor {
        return UIColor("#222222")
    }
    
    static var listResultSectionColor: UIColor = UIColor("#999a9c")
    static var listResultItemColor: UIColor = UIColor("#4a4a4a")
    
    static func boostOuterPulsingColor(backgroundStyle: ThemeBackgroundStyle = .default) -> UIColor! {
        switch backgroundStyle {
        case .default:
            return UIColor("#b2dce1")
        case .lightContent:
            return UIColor("#efa5a3")
        }
    }
    
    static func boostInnerPulsingColor(backgroundStyle: ThemeBackgroundStyle = .default) -> UIColor! {
        switch backgroundStyle {
        case .default:
            return UIColor("#bfeaf0")
        case .lightContent:
            return UIColor("#f5c1bf")
        }
    }
}
