//
//  UIView+Constraint.swift
//  Geofence
//
//  Created by Wan Rahafiz Wan Abdul Rahim on 30/10/2019.
//  Copyright © 2019 Wan Rahafiz Wan Abdul Rahim. All rights reserved.
//

import Foundation
import UIKit

extension UIView {
    func addBoundedConstraint(to: UIView, insets: UIEdgeInsets) {
        self.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
        NSLayoutConstraint.activate([
            self.leadingAnchor.constraint(
                equalTo: to.leadingAnchor,
                constant: insets.left
            ),
            self.trailingAnchor.constraint(
                equalTo: to.trailingAnchor,
                constant: insets.right
            ),
            self.topAnchor.constraint(
                equalTo: to.topAnchor,
                constant: insets.top
            ),
            self.bottomAnchor.constraint(
                equalTo: to.bottomAnchor,
                constant: insets.bottom
            )
            ])
    }
}
