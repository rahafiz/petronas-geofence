//
//  ThemeManager.swift
//  Geofence
//
//  Created by Wan Rahafiz Wan Abdul Rahim on 28/10/2019.
//  Copyright © 2019 Wan Rahafiz Wan Abdul Rahim. All rights reserved.
//

import UIKit

enum ThemeBackgroundStyle: Int {
    case `default`      //
    case lightContent   // For use on dark background
}

enum TextStyle: Int {
    case bst_t1
    case bst_t2
    case bst_st1
    case bst_st2
    case bst_st3
    case bst_b1
    case bst_b2
    case bst_b3
    case bst_b1_bold
    case bst_b2_bold
    case bst_b3_bold
    case bst_alert1
    case bst_alert2
    case bst_alert3
    case bst_btn_action
    case bst_btn_action_secondary
    case bst_hint1
    case bst_hint2
    case title
    case content
    case contentHighlighted
    case contentLink
    case inputTitle
    case inputTxtField
    case popUpTitle
    case popUpHeaderTitle
    case popUpContent
    case popUpContentHighlighted
    case popUpContentLink
    case tipPopUpTitle
    case tipPopUpContent
    case t1
    case t2
    case t3
    case t4
    case t5
    case t6
    case t7
    case t8
    case t9
    case t10
    case t11
    case t12
    case t13
    case t14
    case t15
    case t16
    case t17
    case t18
    case t19
    case t20
}

class ThemeManager: NSObject {
    struct Metrics {
        static let MinimumSectionHeight: CGFloat = {
            if #available(iOS 9.0, *) {
                return CGFloat.leastNonzeroMagnitude
            } else {
                return 1.1
            }
        }()
        
        static func sideMargin() -> CGFloat {
            return 24.0
        }
    }
    
    // MARK: - Shadow
    static func addDefaultShadow(_ view: UIView, cornerRadius: CGFloat? = 8.0) {
        view.layer.shadowColor = UIColor(red: 120/255, green: 120/255, blue: 120/255, alpha: 0.2).cgColor
        view.layer.shadowRadius = 2
        view.layer.shadowOffset = CGSize(width: 0.0, height: 3.0)
        view.layer.shadowOpacity = 1
        view.layer.shouldRasterize = true
        view.layer.rasterizationScale = UIScreen.main.scale
        
        if let cornerRadius = cornerRadius {
            view.layer.cornerRadius = cornerRadius
            view.clipsToBounds = true
        }
    }
    
    static func applyShadow(view: UIView,
                            shadowOffset: CGSize = CGSize.zero,
                            shadowColor: CGColor = UIColor.darkGray.cgColor,
                            shadowOpacity: Float = 0.6,
                            shadowRadius: CGFloat? = 5.0) {
        view.layer.shadowOffset = shadowOffset
        view.layer.shadowColor = shadowColor
        view.layer.shadowOpacity = shadowOpacity
        view.layer.shadowRadius = shadowRadius ?? view.layer.shadowRadius
    }
    
    static func addLineSpacingString(text: String, lineSpacing: CGFloat = 5, fontType: UIFont, textColor: UIColor?) -> NSAttributedString? {
        if text == "" {
            return nil
        }
        
        let attributedString = NSMutableAttributedString(string: text)
        
        // *** Create instance of `NSMutableParagraphStyle`
        let paragraphStyle = NSMutableParagraphStyle()
        
        // *** set LineSpacing property in points ***
        paragraphStyle.lineSpacing = lineSpacing // Whatever line spacing you want in points
        
        // *** Apply attribute to string ***
        attributedString.addAttribute(NSAttributedString.Key.paragraphStyle, value: paragraphStyle, range: NSMakeRange(0, attributedString.length))
        attributedString.addAttribute(NSAttributedString.Key.font,
                                      value: fontType,
                                      range: NSMakeRange(0, attributedString.length))
        
        if let _textColor = textColor {
            attributedString.addAttribute(
                NSAttributedString.Key.foregroundColor,
                value: _textColor,
                range: NSMakeRange(0, attributedString.length)
            )
        }
        
        return attributedString
    }
    
    // NOTE: Don't rely on this method too much. Converting HTML to AttributedString should only be done for complex cases
    // This is a performance intensive function and its usage should be minimized.
    // MARK: - HTML attributed string transformer
    static func htmlAttributedString(text: String?,
                                     alignment: NSTextAlignment = .left,
                                     regularFont: UIFont,
                                     boldFont: UIFont,
                                     textColor: UIColor?,
                                     lineSpacing: CGFloat? = nil) -> NSAttributedString? {
        do {
            guard let t = text else { return nil }
            guard t.count > 0 else { return nil }
            
            let replacedText = t.replacingOccurrences(of: "\n", with: "<br>")
            guard let data = replacedText.data(using: String.Encoding.unicode) else { return nil }
            
            let options: [NSAttributedString.DocumentReadingOptionKey : Any] = [
                NSAttributedString.DocumentReadingOptionKey.documentType: NSAttributedString.DocumentType.html
            ]
            
            let attrStr = try NSMutableAttributedString.init(
                data: data,
                options: options,
                documentAttributes: nil
            )
            
            if alignment != .left && lineSpacing == nil {
                if let paragraphStyle = NSParagraphStyle.default.mutableCopy() as? NSMutableParagraphStyle {
                    paragraphStyle.alignment = alignment
                    paragraphStyle.lineSpacing = 6
                    
                    attrStr.addAttributes(
                        [NSAttributedString.Key.paragraphStyle: paragraphStyle],
                        range: NSRange.init(location: 0, length: attrStr.length)
                    )
                }
            }
            
            if let lineSpacing = lineSpacing {
                // *** Create instance of `NSMutableParagraphStyle`
                let paragraphStyle = NSMutableParagraphStyle()
                
                // *** set LineSpacing property in points ***
                paragraphStyle.lineSpacing = lineSpacing // Whatever line spacing you want in points
                
                // *** Apply attribute to string ***
                attrStr.addAttribute(NSAttributedString.Key.paragraphStyle, value: paragraphStyle, range: NSMakeRange(0, attrStr.length))
            }
            
            attrStr.beginEditing()
            attrStr.enumerateAttribute(NSAttributedString.Key.font, in: NSMakeRange(0, attrStr.length), options: .init(rawValue: 0)) {
                (value, range, _) in
                if let font = value as? UIFont {
                    if let _ = font.fontName.lowercased().range(of: "bold") {
                        attrStr.addAttribute(NSAttributedString.Key.font,
                                             value: boldFont,
                                             range: range)
                    } else {
                        attrStr.addAttribute(NSAttributedString.Key.font,
                                             value: regularFont,
                                             range: range)
                    }
                    
                    if let _textColor = textColor {
                        attrStr.addAttribute(
                            NSAttributedString.Key.foregroundColor,
                            value: _textColor,
                            range: range
                        )
                    }
                }
            }
            
            attrStr.endEditing()
            
            return attrStr
        } catch let error {
        }
        
        return nil
    }
    
    // MARK: - UINavigationBarButton
    static func defaultBackBarButton(barStyle: UIStatusBarStyle = .default, target: AnyObject?, action: Selector?) -> UIBarButtonItem {
        let img: UIImage? = {
            switch barStyle {
            case .lightContent:
                return UIImage(named: "back-white")?
                    .withRenderingMode(.alwaysOriginal)
            case .default:
                return UIImage(named: "back")?
                    .withRenderingMode(.alwaysOriginal)
            case .blackOpaque:
                return UIImage(named: "back-white")?
                    .withRenderingMode(.alwaysOriginal)
            case .darkContent:
                return UIImage(named: "back-white")?
                    .withRenderingMode(.alwaysOriginal)
            }
        }()
        let backBtn = UIBarButtonItem(
            image: img,
            style: .plain,
            target: target,
            action: action
        )
        
        return backBtn
    }
    
    static func defaultCloseBarButton(barStyle: UIStatusBarStyle = .default, target: AnyObject?, action: Selector?) -> UIBarButtonItem {
        let img: UIImage? = {
            switch barStyle {
            case .lightContent:
                return UIImage(named: "close")?
                    .withRenderingMode(.alwaysTemplate)
            case .default:
                return UIImage(named: "close")?
                    .withRenderingMode(.alwaysOriginal)
            case .blackOpaque:
                return UIImage(named: "close")?
                    .withRenderingMode(.alwaysOriginal)
            case .darkContent:
                return UIImage(named: "close")?
                    .withRenderingMode(.alwaysOriginal)

            }
        }()
        let backBtn = UIBarButtonItem(
            image: img,
            style: .plain,
            target: target,
            action: action
        )
        backBtn.tintColor = UIColor.white
        
        return backBtn
    }
    
 

    
    // MARK: - Buttons
    static func applyBorder(btn: UIButton,
                            cornerRadius: CGFloat? = nil,
                            borderColor: UIColor? = nil,
                            borderWidth: CGFloat? = nil) {
        let cornerRadius = cornerRadius ?? btn.frame.height / 2.0
        let borderColor =  borderColor ?? btn.currentTitleColor
        let borderWidth = borderWidth ?? 1.0
        
        btn.layer.borderColor = borderColor.cgColor
        btn.layer.borderWidth = borderWidth
        btn.layer.cornerRadius = cornerRadius
        btn.clipsToBounds = true
    }
    
    
    // MARK: - Toolbars
    static func doneToolbar(text: String?, target: Any?, action: Selector?) -> UIToolbar {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 0, height: 50))
        doneToolbar.barStyle = UIBarStyle.default
        
        let doneTxt = text ?? "Done"
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: doneTxt, style: UIBarButtonItem.Style.done, target: target, action: action)
        
        var items = [UIBarButtonItem]()
        items.append(flexSpace)
        items.append(done)
        
        doneToolbar.items = items
        
        return doneToolbar
    }
    
}

struct PriceFormatter {
    static let defaultFormatter: NumberFormatter = {
        let formatter: NumberFormatter = NumberFormatter()
        
        formatter.numberStyle = .currency
        formatter.currencyCode = "MYR"
        formatter.currencySymbol = "RM"
        
        return formatter
    }()
    
    static func format(amount: Double ,
                       currencyCode: String = "MYR",
                       currencySymbol: String = "RM ",
                       locale: NSLocale = NSLocale.init(localeIdentifier: "en_US"),
                       maxFractionDigits: Int = 2,
                       alwaysShowDecimal: Bool = false) -> String? {
        let formatter: NumberFormatter = defaultFormatter.copy() as! NumberFormatter
        let amountNumber = NSDecimalNumber.init(value: amount)
        let flagZeroFractionDigits: Bool = {
            return amountNumber.doubleValue.truncatingRemainder(dividingBy: 1.0) == 0
        }()
        
        formatter.currencyCode = currencyCode
        formatter.currencySymbol = currencySymbol
        formatter.maximumFractionDigits = alwaysShowDecimal ? maxFractionDigits : (flagZeroFractionDigits ? 0 : maxFractionDigits)
        
        return formatter.string(from: amountNumber)
    }
    
    static func format(amount: String,
                       currencyCode: String = "MYR",
                       currencySymbol: String = "RM ",
                       locale: NSLocale = NSLocale.init(localeIdentifier: "en_US"),
                       maxFractionDigits: Int = 2,
                       alwaysShowDecimal: Bool = false) -> String? {
        let formatter: NumberFormatter = defaultFormatter.copy() as! NumberFormatter
        let amountNumber = NSDecimalNumber.init(string: amount,
                                                locale: locale)
        let flagZeroFractionDigits: Bool = {
            return amountNumber.doubleValue.truncatingRemainder(dividingBy: 1.0) == 0
        }()
        
        
        formatter.currencyCode = currencyCode
        formatter.currencySymbol = currencySymbol
        formatter.maximumFractionDigits = alwaysShowDecimal ? maxFractionDigits : (flagZeroFractionDigits ? 0 : maxFractionDigits)
        
        return formatter.string(from: amountNumber)
    }
    
    static func format(amount: Int,
                       offset: Int = 2,
                       currencySymbol: String = "RM ",
                       maxFractionDigits: Int = 2,
                       alwaysShowDecimal: Bool = false) -> String? {
        let formatter: NumberFormatter = defaultFormatter.copy() as! NumberFormatter
        let offsetDivider = NSDecimalNumber.init(value: pow(10, Double(offset)))
        let offsetAmount = NSDecimalNumber.init(value: amount).dividing(by: offsetDivider)
        let flagZeroFractionDigits: Bool = {
            return offsetAmount.doubleValue.truncatingRemainder(dividingBy: 1.0) == 0
        }()
        
        formatter.currencySymbol = currencySymbol
        formatter.maximumFractionDigits = alwaysShowDecimal ? maxFractionDigits : (flagZeroFractionDigits ? 0 : maxFractionDigits)
        
        return formatter.string(from: offsetAmount)
    }
}

