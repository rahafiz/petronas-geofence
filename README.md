# Petronas-GeoFence

This repository shows how to track your geofence location using swift and model-view-view-model(mvvm) design pattern

# How to run the project

1. After cloning the project, you need to run pod install before build the app.

# How to use the Petronas-GeoFence app.

1. First add location that you are desire to track when enter or exit.
2. Then, after successfully add the location, everytime you enter and leave the location, you will get notify.
3. This app is developed to track the geofence for petronas fuel station. All location already been predefined. Currently its only have 6 location and will be updated time to time.

# Update

1. Fixed compatibility issue with swift 5 and iOS 13